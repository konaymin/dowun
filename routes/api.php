<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

/**
 * Api Routes ( Version 1 )
 */
Route::group([
    'as' => 'api.v1',
    'namespace' => 'Api',
    'prefix' => 'v1'
], function () {
    include ('api/v1.php');
});
